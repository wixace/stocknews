﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentItem : MonoBehaviour
{
    public Text TitleText;

    public Text InfoText;

    public string ContentText;

    public int CategoryID { get; set; }
    
    public int ContentId { get; set; }
    
    public int AuthorId { get; set; }

    public Button Button;

    void Awake()
    {
        Button.onClick.AddListener((() => { ContentManager.Instance.LoadContent(this); }));
    }

    public void SetContent(string title, string info, string content,int authorId)
    {
        TitleText.text = title;
        InfoText.text = info;
        ContentText = content;
        AuthorId = authorId;
    }
}