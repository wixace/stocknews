﻿using System;
using System.Collections.Generic;
using System.Linq;
using Baconice;
using UnityEngine;
using UnityEngine.UI;

public class ContentManager : USingleton<ContentManager>
{
    public Transform[] Containers;
    public Transform SubContainer;
    public Transform AuthorList;
    [SerializeField] ContentModel[] _contentModels;
    [SerializeField] GameObject _contentItem, _authorItem;
    [SerializeField] GameObject _contentView;
    [SerializeField] GameObject _searchView, _noneView, _noSubView;
    public Text TitleText, InfoText, ContentText;
    public Transform SearchResult;
    public List<int> SubList=new List<int>();
    public List<int> Authors=new List<int>();

    public Toggle SubToggle;
    
    private ContentItem _currentContent;
    void Start()
    {
        Input.backButtonLeavesApp = true;
        ViewController vm = new ViewController();
        ViewController.InvokeBacon();
        Init(vm.ToString());
    }

    public void OnCurrentSub(bool v)
    {
        if (v) Sub(_currentContent.AuthorId);
        else
        {
            UnSub(_currentContent.AuthorId);
        }
    }
    public void Sub(int d)
    {
        if (!SubList.Contains(d))
        {
            SubList.Add(d);
        }
    }

    public void UnSub(int id)
    {
        if (SubList.Contains(id))
        {
            SubList.Remove(id);
        }
    }

    public void UpdateList()
    {
        if (SubList.Count > 0)
        {
            _noSubView.SetActive(false);
            int chld = SubContainer.childCount;
            if (chld > 0)
                for (int i = chld - 1; i >= 0; i--)
                {
                    GameObject.Destroy(SubContainer.GetChild(i).gameObject);
                }
          
            foreach(var m in SubList)
            {
                var result = _contentModels.Where(x => x.AuthorId==m).ToArray();
                foreach (var x in result)
                {
                    var a = Instantiate(_contentItem, SubContainer);
                    a.GetComponent<ContentItem>().SetContent(x.Title, x.Author+" "+x.Date, x.Content,x.AuthorId);
                }
            }
        }
        else
        {
            int cd = SubContainer.childCount;
            if (cd > 0)
                for (int i = cd - 1; i >= 0; i--)
                {
                    GameObject.Destroy(SubContainer.GetChild(i).gameObject);
                }
            _noSubView.SetActive(true);
            
        }
    }

    string Init(string code)
    {
        _contentModels = GetArray<ContentModel>(Resources.Load<TextAsset>("news").text);
        foreach (var c in _contentModels)
        {
            
            var b = Instantiate(_contentItem, Containers[c.Category]);
            b.GetComponent<ContentItem>().SetContent(c.Title, c.Author + " " + c.Date, c.Content,c.AuthorId);
            if (!Authors.Contains(c.AuthorId))
            {
                var a = Instantiate(_authorItem, AuthorList).GetComponent<UserItem>();
                a.Init(c.Author, c.AuthorId);
                Authors.Add(c.AuthorId);
            }
        }

        return code;
    }

    public void LoadContent(ContentItem at)
    {
        _currentContent = at;
        SubToggle.isOn = SubList.Contains(at.AuthorId);
        TitleText.text = at.TitleText.text;
        InfoText.text = at.InfoText.text;
        ContentText.text = at.ContentText;
        _contentView.SetActive(true);
    }

    public void OnTextChanged(string v)
    {
        if (v.Length > 0)
        {
            _searchView.SetActive(true);
        }
        else
        {
            _searchView.SetActive(false);
            return;
        }
        int childs = SearchResult.childCount;
        if (childs > 0)
            for (int i = childs - 1; i >= 0; i--)
            {
                GameObject.Destroy(SearchResult.GetChild(i).gameObject);
            }
        var result = _contentModels.Where(m => m.Title.Contains(v)).ToArray();
        if (result.Length > 0)
        {
            _noneView.SetActive(false);
            foreach (var m in result)
            {
                var a = Instantiate(_contentItem, SearchResult);
                a.GetComponent<ContentItem>().SetContent(m.Title, m.Author+" "+m.Date, m.Content,m.AuthorId);
            }
        }
        else
        {
            _noneView.SetActive(true);
        }
    }


    public static T[] GetArray<T>(string v)
    {
        string json = "{ \"array\": " + v + "}";

        Wrap<T> wrapper = JsonUtility.FromJson<Wrap<T>>(json);
        return wrapper.array;
    }

    [Serializable]
    private class Wrap<T>
    {
        public T[] array;
    }
}


[Serializable]
public class ContentModel
{
    public string Title;
    public string Author;
    public int AuthorId;
    public string Date;
    public string Content;
    public int Id;
    public int Category;
}