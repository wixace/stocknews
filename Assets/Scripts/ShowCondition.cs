﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShowCondition : MonoBehaviour
{

    public InputField InputField;


    public UnityEvent Event;

    public GameObject ErrorView;
   
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if(InputField.text.Length<=0)
                ErrorView.SetActive(true);
            else
                Event?.Invoke();
        });
    }

  
}
