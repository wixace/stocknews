﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabElement : MonoBehaviour
{
   private Image _image;

   private Text _text;

   public int Id;
   
   private void Awake()
   {
      _image = GetComponentInChildren<Image>();
      _text = GetComponentInChildren<Text>();
      GetComponent<Button>().onClick.AddListener(() => { GetComponentInParent<TabSwitcher>().Select(Id); });
   }

   public void Show()
   {
      _image.color = new Color(1f, 0.67f, 0.23f);
      _text.color= new Color(1f, 0.67f, 0.23f);
   }

   public void Hide()
   {
      _image.color = Color.black;
      _text.color= Color.black;
   }
   
}
