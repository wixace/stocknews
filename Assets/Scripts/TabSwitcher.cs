﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabSwitcher : MonoBehaviour
{
    public TabElement[] Tabs;

    public GameObject[] Views;
    
    public int SelectedId;
    void Start()
    {
        Tabs[SelectedId].Show();
    }

    public void Select(int id)
    {
        Tabs[SelectedId].Hide();
        Views[SelectedId].SetActive(false);
        SelectedId = id;
        Tabs[SelectedId].Show();
        Views[SelectedId].SetActive(true);
    }
}
