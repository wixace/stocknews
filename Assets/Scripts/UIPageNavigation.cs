﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPageNavigation : MonoBehaviour
{
    public UIPageElement CurrentSelected;

    public void Select(UIPageElement gameObject)
    {
        if (CurrentSelected == gameObject) return;
        CurrentSelected.Target.SetActive(false);
        CurrentSelected.Image.gameObject.SetActive(false);
        CurrentSelected = gameObject;
        CurrentSelected.Target.SetActive(true);
        CurrentSelected.Image.gameObject.SetActive(true);
    }
}
