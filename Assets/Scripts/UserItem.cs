﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserItem : MonoBehaviour
{

    public Text _authorText;

    public Toggle Toggle;

    public void Init(string author,int id)
    {
        Id = id;
        _authorText.text = author;
    }

    private void OnEnable()
    {
        Toggle.isOn = ContentManager.Instance.SubList.Contains(Id);
    }

    public int Id { get; set; }
    // Start is called before the first frame update
    public void OnValueChanged(bool v)
    {
        if (v)
            ContentManager.Instance.Sub(Id);
        else
        {
            ContentManager.Instance.UnSub(Id);
        }
    }

  
    
    
}
